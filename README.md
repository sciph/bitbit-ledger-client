 # BITBIT LEDGER Client API Documentation
 ```mermaid
graph TD;
  Header-->JWT;
  Payload-->JWT ;
  Signature-->JWT;
```


## JSON Web Tokens (JWT)
JSON Web Token is based on RFC 7519, an open standard to secure information transmitted between two parties as JSON object.   
It uses HMAC algorithm to digitally sign the information that can be verified and trusted. Constructed with three parts: Header, Payload and Signature

**<span style='color:green'>JWT Header</span>**
````Javascript
{
  "alg": "HS256",
  "typ": "JWT"
}
````

**<span style='color:blue'>JWT Payload</span>**
````Javascript
{
  "account_id": "1234567890",
  "app_code": "BITBIT",
  "currency": "ETH"
}
````

**<span style='color:violet'>JWT Signature</span>**
````Javascript
HMACSHA256(
  base64UrlEncode(header) + "." +
  base64UrlEncode(payload),
  "your 256 bit secret"
)
````

**Constructed Token String**
<pre>
<span style='color:green'>eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9</span>.<span style='color:blue'>eyJhY2NvdW50X2lkIjoiMTIzNDU2Nzg5MCIsImFwcF9jb2RlIjoiQklUQklUIiwiY3VycmVuY3kiOiJFVEgifQ</span>.<span style='color:violet'>MQgcpR-DHmRUlO_cXxMXb5y9E6VFzOK_H5FOB_LHa0s</span>
</pre>

## Example API access parameters for this documentation

`https://api.sciblockchain.tech/bitbit-ledger/v1/wallets`

| ROOT URI | Version | Module |
| -------- | ------- | ------ |
| `https://api.sciblockchain.tech/bitbit-ledger` | `/v1` | `/wallets` |


## API Commands Table
| &nbsp; Actions | Create | Read | Update | List |
| --------------- | ------ | ---- | ------ | ------ |
| /accounts |  <ul><li>app_code</li><ul> |   <ul><li>account_id</li><ul> |  <ul><li>account_id</li><ul> | <span style="color:red">**N/A**</span> |
| /wallets |  <ul><li>account_id</li><li>app_code</li><li>currency</li><ul> |  <ul><li>wallet_id</li><ul> |  <ul><li>wallet_id</li><ul> | <span style="color:red">**N/A**</span> |
| /walletaddress | <ul><li>wallet_id</li><ul> | <ul><li>walletaddress_id</li><ul> | <ul><li>walletaddress_id</li><ul> | <ul><li>wallet_id</li><ul> |
| /transactions | <ul><li>wallet_id</li><li>inputs</li><li>outputs</li><li>overhead</li><li>total_amount</li><ul> | <ul><li>transaction_id</li><ul> | <span style="color:orange">**Sign**</span> <ul><li>transaction_id</li><ul> | <span style="color:red">**N/A**</span> |
| /auth | <span style="color:red">**N/A**</span> | <span style="color:green">**Yes**</span> | <span style="color:red">**N/A**</span> | <span style="color:red">**N/A**</span> |




## Accounts : /accounts
### Create
````
POST /bitbit-ledger/accounts HTTP/1.1
Host: api.sciblockchain.tech
cache-control: no-cache

// Generate Token with JWT Payload
{
  "iss": "<Assigned API key>",
  "iat": <Issued timestamp>,
  "action": "create",
  "data": {
    "type": "account",
    "app_code": "BITBIT"
  }
}
````

#### Response
````
{
    "data": {
        "type": "account",
        "item":{
          "id": "<account_id>",
          "app_code": "BITBIT",
          "secret_key": "<secret key string>",
          "seed_hex": "<encrypted seed hex>",
          "created_by": {
              "api_key": "<api key id>"
          },
          "datetime": {
              "created": "<date and time>",
              "updated": "<date and time>"
          },
          "mnemonic": "<one-time 12 word strings>"  
        }
    }
}
````


### Read

````
POST /bitbit-ledger/accounts HTTP/1.1
Host: api.sciblockchain.tech
cache-control: no-cache

// Generate Token with JWT Payload
{
  "iss": "<Assigned API key>",
  "iat": <Issued timestamp>,
  "action": "read",
  "data": {
    "type": "account",
    "account_id": "<account_id>"
  }
}

````


### Update
````
POST /bitbit-ledger/accounts HTTP/1.1
Host: api.sciblockchain.tech
cache-control: no-cache

// Generate Token with JWT Payload
{
  "iss": "<Assigned API key>",
  "iat": <Issued timestamp>,
  "action": "update",
  "data": {
    "type": "account",
    "account_id": "<account_id>",
    <field>: <new value>
  }
}

````


## Wallets
### Create
````
POST /bitbit-ledger/wallets HTTP/1.1
Host: api.sciblockchain.tech
cache-control: no-cache


// Generate Token with JWT Payload
{
  "iss": "<Assigned API key>",
  "iat": <Issued timestamp>,
  "action": "create",
  "data": {
    "type": "wallet",
    "account_id": "<account id>",
    "app_code": "BITBIT",
    "currency": "BTC"
  }
}
````


### Read
````
POST /bitbit-ledger/wallets HTTP/1.1
Host: api.sciblockchain.tech
cache-control: no-cache

// Generate Token with JWT Payload
{
  "iss": "<Assigned API key>",
  "iat": <Issued timestamp>,
  "action": "read",
  "data": {
    "type": "wallet",
    "wallet_id": "<wallet id>"
  }
}

````

### List wallet address
````
POST /bitbit-ledger/wallets HTTP/1.1
Host: api.sciblockchain.tech
cache-control: no-cache

// Generate Token with JWT Payload
{
  "iss": "<Assigned API key>",
  "iat": <Issued timestamp>,
  "action": "list",
  "data": {
    "type": "wallet",
    "account_id": "<account id>"
  }
}
````


## Wallet Address
### Create
````
POST /bitbit-ledger/walletaddress HTTP/1.1
Host: api.sciblockchain.tech
cache-control: no-cache


// Generate Token with JWT Payload
{
  "iss": "<Assigned API key>",
  "iat": <Issued timestamp>,
  "action": "create",
  "data": {
    "type": "walletaddress",
    "wallet_id": <wallet_id>
  }
}
````

### Read & Check balance
````
POST /bitbit-ledger/walletaddress HTTP/1.1
Host: api.sciblockchain.tech
cache-control: no-cache

// Generate Token with JWT Payload
{
  "iss": "<Assigned API key>",
  "iat": <Issued timestamp>,
  "action": "read",
  "data": {
    "type": "walletaddress",
    "walletaddress_id": "<walletaddress_id>"
  }
}
````

### List wallet address
````
POST /bitbit-ledger/walletaddress HTTP/1.1
Host: api.sciblockchain.tech
cache-control: no-cache

// Generate Token with JWT Payload
{
  "iss": "<Assigned API key>",
  "iat": <Issued timestamp>,
  "action": "list",
  "data": {
    "type": "walletaddress",
    "wallet_id": "<wallet_id>"
  }
}
````



## Transaction

### Create Transaction

```
POST /bitbit-ledger/transactions HTTP/1.1
Host: api.sciblockchain.tech
cache-control: no-cache

// Generate Token with JWT Payload
{
  "iss": "<Assigned API key>",
  "iat": <Issued timestamp>,
  "action": "create",
  "data": {
    "type": "transaction",
    "wallet_id": <wallet_id>,
    "inputs": [
      {
        "walletaddress_id": <walletaddress_id>,
        "walletaddress": <wallet address>,
        "amount": <amount in numbers>
      },
      {
        "walletaddress_id": <walletaddress_id>,
        "walletaddress": <wallet address>,
        "amount": <amount in numbers>
      }
    ],
    "outputs": [
      {
        "wallet_id": <wallet_id>,
        "walletaddress": <wallet address>,
        "amount": <amount in numbers>
      }
    ],
    "overhead": [
      {
        "gas": <cost in numbers>,
      }
    ],
    "summary": {
      "inputs_total": <amount in numbers>,
      "output_total": <amount in numbers>,
      "overhead_total": <amount in numbers>,
      "total_amount": <amount in numbers>
    }

  }
}
````


### Read Transaction
````
POST /bitbit-ledger/transactions HTTP/1.1
Host: api.sciblockchain.tech
cache-control: no-cache

// Generate Token with JWT Payload
{
  "iss": "<Assigned API key>",
  "iat": <Issued timestamp>,
  "action": "read",
  "data": {
    "type": "transaction",
    "transaction_id": <transaction_id>
  }
}
````

### List Transaction

````
POST /bitbit-ledger/transactions HTTP/1.1
Host: api.sciblockchain.tech
cache-control: no-cache

// Generate Token with JWT Payload
{
  "iss": "<Assigned API key>",
  "iat": <Issued timestamp>,
  "action": "list",
  "data": {
    "type": "transaction",
    "wallet_id": <wallet_id>
  }
}
````





## Reference Table

### App Codes

Short Code | Description |
:--: | :-- |
BITBIT | Bitbit Wallet App |
REBIT | Rebit Remittance |
BUYBITCOIN | BuyBitcoin Wallet |


### Currency

Short Code | Description |
:--: | :-- |
BTC | Bitcoin |
BCH | Bitcoin Cash |
ETH | Ethereum |
ETC | Ethereum Classic |

### Transaction Stages

Short Code | Description
:--: | :--
DRAFT | Drafted Transaction
OPEN | transaction to be processed
PENDING | Waiting for confirmation
PROCESSING | Confirmed and on-going processing
FAILED | Something Wong Happened during Processing
CANCELED | Canceled transaction
CLOSED | Finished Job


### Job Service Stages

Short Code | Description |
:--: | :-- |
OPEN | Open or New Jobs |
PROCESSING | On-going Job to be processed |
FAILED | Something Wong Happened during Processing |
CLOSED | Finished Job |

### Successful Response Codes
Code | Message |
:--: | :-- |
200 | Ok |
201 | Created |
202 | Accepted |
204 | Successful but no Content |
205 | Reset content |
205 | Partial content |


### Request Error Codes
Code | Message |
:--: | :-- |
400 | Bad Request |
401 | Unauthorized |
403 | Forbidden |
404 | Not Found |
408 | Request Timeout |
429 | Too Many Request |


### System Error Codes
Code | Message |
:--: | :-- |
500 | Internal Server Error |
501 | Not Implemented |
502 | Bad Gateway |
503 | Service Unavailable |
504 | Gateway Timeout |




## Roadmap
Version | Date | &nbsp; |
:------ | :--: | :----------- |
0.01 Beta Release | 2018-12-28 | <ul><li>HD Wallet Compatibility</li> <li>BTC Multisig Wallts</li> <li>ETH/ERC20 Wallet Transactions</li>  <li>BTC Wallet Transactions</li> </ul> |
Beta Release | 2018-12-05 | <ul><li>test</li>  <li>test</li> </ul> |
Alpha Release | 2018-11-05 | <ul><li>test</li>  <li>test</li> </ul> |

## Resources
* https://tools.ietf.org/html/rfc7519
* https://jwt.io/
* https://github.com/auth0/node-jsonwebtoken
* https://www.restapitutorial.com/httpstatuscodes.html
* https://medium.com/@bhanushali.mahesh3/building-a-restful-crud-api-with-node-js-jwt-bcrypt-express-and-mongodb-4e1fb20b7f3d
